%*****************************************
\chapter{L'interferometro di Talbot-Lau}\label{ch:interferometro}

\section{Reticolo analizzatore e formazione delle
immagini}\label{sec:imaging}
Il principio fondamentale del funzionamento dell'interferometro, determinato dal fenomeno
Talbot, consiste nella formazione dell'immagine del reticolo alla distanza
di Lohmann. Un oggetto che introduca una rifrazione dei raggi X produce
quindi uno spostamento laterale delle frange, poich\'e per piccoli angoli la deviazione
angolare di un'onda piana $\alpha$ \`e proporzionale al gradiente della fase
$\varphi(\vec{x}) = \vec{k}\cdot\vec{x}$. Se il reticolo introduce una
struttura periodica lungo l'asse $x$ possiamo quindi osservare lo
spostamento della figura di interferenza lungo questo asse, che corrisponde
alla variazione nella fase dei raggi X introdotta dal campione. Questo
introduce infatti un angolo di rifrazione $\alpha$:
\begin{equation}
    \alpha = -\frac{1}{k} \frac{\partial\varphi}{\partial
    x}.\label{eq:refraction.angle}
\end{equation}

Tuttavia, il periodo del reticolo deve essere dell'ordine dei micrometri per
ottenere una buona sensibilit\`a dello strumento
(paragrafo~\ref{par:sensibilita}), e i rivelatori disponibili
non possono risolvere dettagli su questa scala. Perci\`o si introduce un
secondo reticolo di assorbimento $\G{2}$, con lo stesso periodo $p_2$ della figura d'interferenza
creata da $\G{1}$, che viene fatto scorrere lungo un periodo in direzione
$x$ (figura~\ref{fig:phase.stepping}): quando le linee di $\G{2}$ sono perfettamente sovrapposte alle frange luminose, il
pixel risulta buio, mentre quando si sovrappongono alle frange scure, la
luminosit\`a trasmessa \`e massima. In generale, si registra la convoluzione
del segnale rettangolare della figura di interferenza con il segnale
rettangolare del reticolo di assorbimento: ne risulta una curva triangolare
nota come curva di \emph{phase stepping}.

\begin{figure}[htb]
    \centering
    \input{gfx/phase_stepping.eepic}
    \caption[Curva di \emph{phase stepping}.]{Il reticolo \G{2} analizza la figura d'interferenza sulla
sinistra scorrendo in direzione perpendicolare alle frange. La curva
d'intensit\`a cos\`i registrata permette di determinare tre parametri per
ogni pixel:
la media $a_0$, l'ampiezza $a_1$ e la fase $\theta$.}
    \label{fig:phase.stepping}
\end{figure}
In realt\`a tenendo conto delle dimensioni e della coerenza finite della
sorgente, tale curva risulta pi\`u vicina ad una sinusoide ed \`e sufficiente considerare la
prima armonica~\cite{Weitkamp2006}
\begin{equation*}
    s(x) = a_0 + a_1 \cos \left(\frac{2 \pi}{p_2} x + \theta\right).
\end{equation*}

Registrando quindi un numero sufficiente di punti lungo questa curva, detti
\emph{punti di fase}, \`e
possibile determinare con un fit i tre parametri $a_0$, $a_1$ e $\theta$.

I parametri ricavati dalla curva registrata in presenza del campione
$s_c$ sono infine confrontati con la curva non perturbata $s_f$ per ricavare tre immagini:
\begin{description}
    \item[assorbimento,] dato dal rapporto $A = a_{0,c} / a_{0,f}$, \`e
        l'intensit\`a della radiazione trasmessa, come nella radiografia
        tradizionale e nella legge di Beer-Lambert~\eqref{eq:beer-lambert}
    \item[fase differenziale,] ovvero la differenza $P = \theta_{c} -
        \theta_f$, che dipende dallo spostamento laterale delle frange
        $\alpha$~\eqref{eq:refraction.angle} alla
        $j$-esima distanza di Lohmann secondo
        \begin{equation*}
            P = 2\pi \frac{D_j}{p_2}\alpha
        \end{equation*}
        ed è quindi proporzionale alla derivata dello spostamento di fase
        introdotto dall'oggetto
    \item[riduzione di visibilit\`a,] anche nota come immagine di
        \emph{scattering} o di \emph{campo oscuro}. Si dice \emph{visibilit\`a} della
        curva il parametro
        $v = 2a_1 / a_0$. Il rapporto
        \begin{equation*}
            B = \frac{v_c}{v_f} =
            \frac{a_{1,c}}{a_{0,c}}\frac{a_{0,f}}{a_{1,f}}
        \end{equation*}
        \`e proporzionale alla larghezza locale della distribuzione angolare
        della diffusione~\cite{Wang2009} e alle disomogeneit\`a su scale di
        lunghezza pi\`u piccole dei pixel del rivelatore~\cite{Kagias},
        ma una sua completa descrizione che tenga conto anche del contributo
        dell'effetto Compton \`e ancora oggetto di studi.
\end{description}

\subsection{Coerenza spaziale e temporale}
Uno dei vantaggi fondamentali dell'interferometro di Talbot-Lau \`e la sua
compatibilit\`a con sorgenti da laboratorio, che hanno uno spettro di
\emph{bremsstrahlung} molto ampio. \`E stato dimostrato~\cite{Weitkamp2005}
che la larghezza dello spettro che contribuisce alla formazione delle
immagini alla $j$-esima distanza di Lohmann \`e
\begin{equation}
    \frac{\Delta \lambda}{\lambda} = \frac{1}{2j - 1}\label{eq:acceptance}
\end{equation}

La coerenza spaziale \`e problematica per una sorgente convenzionale.
Semplici considerazioni geometriche~\cite{Weitkamp2006} mostrano che un
normale tubo a raggi X ha una sorgente troppo estesa e non soddisfa i
requisiti di coerenza spaziale. Non sarebbe quindi possibile osservare
l'interferenza, ma si pu\`o ovviare a questo problema introducendo un terzo
reticolo~\cite{Pfeiffer2006,Chen2010a,Momose2009a} vicino al generatore di
raggi X, $\G{0}$, che crea una sequenza di sorgenti molto sottili
individualmente coerenti. Perch\'e le figure d'interferenza generate dalla
sorgenti individuali si sovrappongano, il periodo di $\G{0}$ deve soddisfare
la condizione
\begin{equation}
    p_0 = p_2 \frac{\ell}{D_j}\label{eq:p0}
\end{equation}
dove $\ell$ la distanza tra $\G{0}$ e $\G{1}$ (figura~\ref{fig:distanze}).
Si dice propriamente \emph{interferometro di Talbot} lo strumento costruito
con i soli due reticoli $\G{1}$ e $\G{2}$ e \emph{interferometro di
Talbot-Lau} quello realizzato con l'aggiunta di $\G{0}$.

\begin{figure}[htb]
    \centering
    \input{gfx/grating_interferometer.eepic}
    \caption[Schema dell'interferometro di Talbot-Lau.]{Schema
    dell'interferometro di Talbot-Lau.}
    \label{fig:distanze}
\end{figure}

\subsection{Ingrandimento geometrico}
Le equazioni introdotte finora assumono una geometria parallela.
Tuttavia, occorre tener conto del fronte d'onda sferico generato dalla
sorgente. A tal fine \`e sufficiente correggere le distanze in modo da
includere l'ingrandimento geometrico~\cite{Engelhardt2008}. Le distanze di
Lohmann sono dunque riscalate:
\begin{equation*}
    D_j^\prime = D_j \frac{1}{1 - \dfrac{D_j}{\ell}}.
\end{equation*}
Il periodo $p_2$ deve essere moltiplicato per lo stesso fattore
\begin{equation}
    p_2^\prime = p_2 \frac{1}{1 -
        \dfrac{D_j}{\ell}}.\label{eq:magnification}
\end{equation}

\section{Sensibilità e visibilit\`a}\label{par:sensibilita}
La varianza del segnale differenziale di fase, se $\sigma_{\text{det}}^2$ \`e
la varianza tipicamente poissoniana data dal rivelatore, \`e data da~\cite{Raupach2011}
\begin{equation}
    \sigma_\alpha = \frac{p_2}{\pi D_j}
    \frac{\sqrt{2}}{v}\sigma_{\text{det}}.\label{eq:variance}
\end{equation}
Per ottenere la massima sensibilit\`a \`e dunque necessario
avere fabbricare reticoli con un periodo $p_2$ il pi\`u piccolo possibile.
La produzione di reticoli con periodi molto piccoli incontra per\`o un limite
tecnologico: se $h$ \`e la profondit\`a delle strutture del reticolo, il
rapporto $R = 2h / p_2$ attualmente raggiungibile \`e circa
60~\cite{David2007,Kenntner2010}, ma lo spessore $h$ non pu\`o essere
ridotto arbitrariamente, in quanto deve essere garantito un assorbimento
efficace della radiazione nei reticoli $G_0$ e $G_2$.
Per una lunghezza totale dell'interferometro $\ell + D_j$ fissata,
si pu\`o riscrivere con la~\eqref{eq:p0} il fattore $p_2/D_j$ da minimizzare
nella~\eqref{eq:variance}
\begin{equation*}
    \frac{p_2}{D_j} = \frac{p_0 + p_2}{\ell + D_j}.
\end{equation*}
Le limitazioni tecnologiche alla fabbricazione di reticoli sono le stesse
per $G_0$ e $G_2$, quindi la sensibilit\`a massima si ottiene per periodi
minimi e uguali tra loro, il che impone, attraverso
la~\eqref{eq:p0} e la~\eqref{eq:magnification} un interferometro simmetrico,
con $\ell = D_j'$ e $p_0 = p_1 = p_2$.

Il secondo fattore determinante nell'equazione~\eqref{eq:variance} \`e la
visibilit\`a~$v$. Si potrebbe infatti pensare di ridurre la varianza
semplicemente
aumentando la distanza $D_j$, ma ci\`o avrebbe due conseguenze negative:
\begin{aenumerate}
    \item l'errore relativo dato dal rivelatore $\sigma_{\text{det}}/N$, per
        $N$ fotoni osservati, \`e proporzionale a $1 / \sqrt{N}$, e ovviamente $N \propto
        D_j^{-2}$
    \item la larghezza dello spettro che contribuisce alla visibilit\`a
        diminuisce con la distanza secondo la~\eqref{eq:acceptance}. Si
        passa da un massimo teorico per la visibilit\`a del
        \SI{26}{\percent}\footnote{Calcolo numerico secondo lo spettro del
            tubo a raggi X del laboratorio simulato con
            SpekCalc~\cite{SpekCalc3}.} per $j = 1$ a circa il $\SI{10}{\percent}$ per
        $j = 5$.
\end{aenumerate}
Tenendo conto di questi fatti, si \`e scelto di realizzare gli
interferometri alla prima distanza di Lohmann $D_1 = p_1^2 / 8 \lambda$.

\subsection{Analisi dati}
Secondo la procedura seguita anche nel resto del testo per l'analisi delle curve di
\emph{phase stepping}, si calcola la trasformata di Fourier discreta
(con algoritmi di \emph{fast Fourier
transform}~\cite{WilliamTeukolsky,CooleyTukey}) della sequenza di punti registrati nello spostamento di
\G{2}. Il primo coefficiente di Fourier \`e dunque $a_0$, il
modulo e la fase del secondo coefficiente sono rispettivamente $a_1$ e
$\theta$.

Tale algoritmo \`e matematicamente equivalente a un fit con il metodo dei
minimi quadrati~\cite{FFTEquivalence} in cui tutti i punti hanno il medesimo peso.
Alternativamente, con una procedura pi\`u impegnativa dal punto di vista
computazionale, si pu\`o introdurre un peso per ogni punto stimato come
l'inverso della varianza poissoniana dovuta al rivelatore.

Per confrontare le due procedure, sono state simulate delle curve
sinusoidali con \num{9} punti e rumore poissoniano $\sigma_i = \sqrt{N_i}$,
dove $N_i$ \`e
il numero di fotoni registrato per l'$i$-esimo punto di fase, al variare dei
parametri $a_0$ e $v$, con \num{100000} curve per ogni valore di questi
parametri.
L'analisi statistica in figura~\ref{fig:stats_phase_visibility_sd} mostra
l'andamento della deviazione standard sulla fase al variare della
visibilit\`a e per tre valori della costante $a_0$. Per valori bassi della
visibilit\`a \`e naturale aspettarsi che i pesi poissoniani siano molto
simili tra loro e quindi la loro introduzione non influenzi il fit. Tuttavia
gli estimatori pesati, che hanno varianza minima per il teorema di
Gauss-Markov~\cite{eadie2006statistical}, possono essere di maggior
rilevanza per esperimenti realizzati ai sincrotroni, dove la visibilit\`a
pu\`o superare anche il \SI{50}{\percent}. Potrebbe essere anche rilevante
in presenza fonti addizionali di rumore qui non citate, come per esempio l'instabilit\`a
della sorgente.

Al contrario, l'errore sulla
visibilit\`a (figura~\ref{fig:stats_visibility_visibility_sd}) non dipende fortemente dalla visibilit\`a
stessa ma dall'intensit\`a media~\cite{Chabior2011}
\begin{equation*}
    \sigma_{a_1} \propto \frac{1}{\sqrt{a_0}}
\end{equation*}
\`E importante notare che, per tempi di esposizione troppo ridotti o
assorbimento molto elevato, l'estimatore della visibilit\`a introduce un
\emph{bias} (simulazione in 
figura~\ref{fig:stats_visibility_visibility_mean}). Infatti la visibilit\`a
\`e calcolata a partire dal modulo del primo coefficiente di Fourier. La
distribuzione di tale valore segue una distribuzione di
Rice~\cite{Chabior2011}, che diventa asimmetrica quando il rumore
poissoniano propagato dalle immagini originali aumenta.
Anche per questo parametro, i due fit sono equivalenti nei valori di
visibilit\`a osservati per i nostri esperimenti.

\begin{figure}[htb]
    \centering
    \begin{subfigure}[b]{\textwidth}
    \centering
    \input{gfx/stats_phase_visibility_sd.pgf}
    \caption{}
    \label{fig:stats_phase_visibility_sd}
    \end{subfigure}
    \begin{subfigure}[b]{\textwidth}
    \centering
    \input{gfx/stats_visibility_visibility_sd.pgf}
    \caption{}
    \label{fig:stats_visibility_visibility_sd}
    \end{subfigure}
    \caption[Varianza dei parametri delle curve di \emph{phase stepping}.]{
    Confronto della varianza dei parametri di
    fase~(\ref{fig:stats_phase_visibility_sd}) e
    visibilit\`a~(\ref{fig:stats_visibility_visibility_sd}) dovuta
    alla varianza poissoniana nel numero di fotoni registrati per ogni punto di
    fase, al variare dell'intensit\`a media e della visibilit\`a.}
\end{figure}
\begin{figure}[htb]
    \centering
    \input{gfx/stats_visibility_visibility_mean.pgf}
    \caption[Estimatore della visibilit\`a]{L'estimatore della visibilit\`a
        mostra un \emph{bias} in caso di bassi tempi di esposizione o
    elevato assorbimento. Non si osserva nessuna differenza tra le due
    modalit\`a di fit.}
    \label{fig:stats_visibility_visibility_mean}
\end{figure}


%************************************************
\chapter{Apparato sperimentale e risultati}\label{ch:esperimenti}
%************************************************
\section{Fabbricazione dei reticoli e geometria \emph{edge-on}}
L'obiettivo dell'esperimento \`e dunque una dimostrazione
dell'applicabilit\`a dell'interferometria di Talbot-Lau alle energie
normalmente usate in medicina nella tomografia e per i controlli di
sicurezza. Sono stati realizzati due interferometri, con energia
nominale di~\SI{100}{\kilo\eV} e~\SI{120}{\kilo\eV} rispettivamente~\cite{Thuering2013e}.

La tecnologia LIGA~\cite{Kenntner2010} usata per la fabbricazione dei
reticoli impone, come ricordato in precedenza, un limite fondamentale al
rapporto tra altezza e profondit\`a delle linee del reticolo. Secondo la
legge di Beer-Lambert~\eqref{eq:beer-lambert}, per ottenere un reticolo 
efficace, le cui linee assorbano il \SI{99}{\percent} della radiazione,
occorre uno spessore d'oro pari a\footnote{Per l'oro,
    $\mu(\SI{100}{\kilo\eV}) =
    \SI{81.75}{\per\centi\metre}$~\cite{Hubbell_Seltzer_1995}.}
\begin{equation*}
    h = - \frac{1}{\mu} \log\left( \frac{I}{I_0} \right) \approx
    \SI{560}{\micro\metre}
\end{equation*}
Il periodo sarebbe quindi circa~\SI{20}{\micro\metre} e non fornirebbe una
sensibilit\`a sufficiente.
In realt\`a le condizioni sarebbero ancora pi\`u proibitive poich\'e con una
sorgente policromatica \`e necessario assorbire anche raggi X di energia
superiore a quella nominale dell'interferometro, nel nostro caso fino
a~\SI{160}{\kilo\eV}.

\begin{figure}[htb]
    \centering
    \includegraphics[width=0.8\textwidth]{gfx/hDPC_setup.png}
    \caption[Illuminazione \emph{edge-on}.]{Schema dell'interferometro Talbot-Lau nella geometria
        \emph{edge-on}. Le strutture dei reticoli sono disposte lungo la
    direzione del fascio e permettono di ottenere profondit\`a
arbitrariamente grandi.}
    \label{fig:schematic}
\end{figure}

La disposizione \emph{edge-on}~\cite{PatentEdgeOn} (figura~\ref{fig:schematic}) permette di superare questo limite e ridurre
il periodo di un ordine di grandezza: il reticolo \`e illuminato
\emph{di lato}, cio\`e
parallelamente alla direzione delle strutture, che quindi in direzione del
fascio possono essere costruite con un rapporto $h/p$ arbitrario. Cos\`i
facendo, le immagini registrate risultano unidimensionali poich\'e
l'estensione verticale del campo visivo \`e ristretta a
\SI{100}{\micro\metre}, e diventa
necessario eseguire una scansione del campione. Questa tecnica ha per\`o un
secondo aspetto positivo, in quanto facilita l'allineamento
delle strutture dei
reticoli lungo un arco di circonferenza, in modo che si adattino al fronte
d'onda sferico anche a grandi angoli rispetto all'asse ottico.

I reticoli sono stati costruiti da Microworks GmbH, con i parametri
specificati in tabella~\ref{tab:gratings}, calcolati in base alle esigenze
esposte nel capitolo precedente. Tutti i reticoli sono realizzati in oro
(ad eccezione del \G{1} per l'energia di \SI{120}{\kilo\eV}, che \`e di nichel), hanno un periodo
di~\SI{2.8}{\micro\metre}, un'altezza di~\SI{100}{\micro\metre} e un
ciclo di lavoro (\emph{duty cycle}\footnote{\`E la frazione di lunghezza di un periodo occupata
dalle strutture del reticolo. In questo caso, il ciclo di lavoro \`e di
\num{0.5}, ovvero ogni periodo di~\SI{2.8}{\micro\metre} \`e diviso in due
parti uguali con~\SI{1.4}{\micro\metre} di metallo seguiti da uno spazio
vuoto o riempito di un polimero di supporto trasparente ai raggi X di
altri~\SI{1.4}{\micro\metre}.}) di \num{0.5}.

\begin{table}[htb]
    \centering
    \begin{tabular}{*4c}
        \toprule
        reticolo & energia (\si{\kilo\eV}) & raggio di curvatura
        (\si{\centi\metre}) & profondit\`a (\si{\micro\metre}) \\
        \midrule
        $G_0$ & \num{100}/\num{120} & \num{23} & \num{800} \\
        $G_1$ & \num{100} & \num{38.8} & \num{42.2} \\
        $G_1$ & \num{120} & \num{42.0} & \num{19.4} \\
        $G_2$ & \num{100} & \num{54.5} & \num{800} \\
        $G_2$ & \num{120} & \num{60.9} & \num{800} \\
        \bottomrule
    \end{tabular}
    \caption{Parametri di fabbricazione dei reticoli.}
    \label{tab:gratings}
\end{table}

\section{Esperimenti a \SI{100}{\kilo\eV} e \SI{120}{\kilo\eV}}
\subsection{Allineamento}
L'interferometro deve essere allineato in un piano con precisione
dell'ordine di~\SI{10}{\micro\metre}. I reticoli devono inoltre rispettare
le distanze relative ed essere il pi\`u possibile paralleli tra loro. Ogni
reticolo \`e provvisto di viti micrometriche su sei assi, per la traslazione
lungo i tre assi cartesiani e la rotazione intorno ad essi.
Con riferimento alla figura~\ref{fig:schematic}, si riportano
i margini di ripetibilit\`a
nell'allineamento effettivamente raggiunti (tabella~\ref{tab:allineamento}).
\begin{table}[htb]
    \centering
    \begin{tabular}{*2c}
        \toprule
        allineamento & ripetibilit\`a\\
        \midrule
        rotazione $x$ & \SI{0.05}{\degree}\\
        rotazione $y$ & \SI{0.003}{\degree}\\
        rotazione $z$ & \SI{0.05}{\degree}\\
        traslazione $x$ & \SI{1}{\milli\metre}\\
        traslazione $y$ & \SI{10}{\micro\metre}\\
        traslazione $z$ & \SI{5}{\milli\metre}\\
        \bottomrule
    \end{tabular}
    \caption{Margini di ripetibilit\`a dell'allineamento dei reticoli. Gli
        assi cartesiani sono orientati come in figura~\ref{fig:schematic}.}
    \label{tab:allineamento}
\end{table}
Il protocollo seguito per l'allineamento \`e diviso in varie fasi:
\begin{enumerate}
    \item posizionamento manuale su una scala graduata lungo $z$ dei reticoli
    \item allineamento della traslazione lungo $x$, con un laser
        precedentemente calibrato
    \item allineamento della traslazione lungo $y$,
        in modo che ciascuno sia centrato nel fascio di raggi X
    \item la rotazione attorno all'asse $z$ \`e corretta quando il bordo
        superiore di ciascun reticolo \`e contenuto nell'altezza di un pixel
        del rivelatore
    \item la rotazione attorno all'asse $y$ e il posizionamento fine lungo
        $z$ dei reticoli di assorbimento si
        correggono massimizzando il flusso di raggi X attraverso i reticoli stessi
    \item la rotazione attorno all'asse $y$ e il posizionamento lungo $z$
        del reticolo \G{1} non possono essere osservate direttamente,
        poich\'e le strutture di questo reticolo non assorbono
        sufficientemente la radiazione. Si deve quindi far riferimento
        ad eventuali frange di Moir\'e che rivelano un allineamento
        imperfetto.
\end{enumerate}
Piccole correzioni residue possono essere infine effettuate massimizzando la
visibilit\`a.

\subsection{Metodi}
Il generatore di raggi X \`e un tubo
COMET MXR-160HP/11 con un voltaggio massimo, sempre usato in questi
esperimenti, di 
$\SI{160}{\kilo\volt}$. 
La macchia focale ha un diametro di circa
$\SI{1}{\milli\metre}$. Il rivelatore \`e costituito da una fotocamera CCD
di Finger Lakes
Instruments e da uno scintillatore di ioduro di cesio (CsI:Ti) di $\SI{600}{\micro
\metre}$ di spessore che converte i raggi X in luce visibile. L'efficienza
dello scintillatore \`e circa del \SI{30}{\percent}. Le dimensioni
effettive dei pixel sono $\SI{80}{\micro \metre}\times\SI{80}{\micro \metre}$.
Il fascio \`e collimato sul piano in cui si trovano i reticoli con due
fenditure di acciaio spesse $\SI{1}{\centi\metre}$, una all'uscita
del generatore con altezza di $\SI{25}{\micro\metre}$ e una prima del
rivelatore con altezza di $\SI{100}{\micro\metre}$.

\subsection{Prestazioni}
La visibilit\`a del sistema, inversamente proporzionale alla deviazione
standard sull'angolo di rifrazione $\alpha$~\eqref{eq:variance}, \`e un
parametro fondamentale per caratterizzare le prestazioni dell'esperimento.
Per ottenere un'alta visibilit\`a \`e evidentemente necessaria la
massima uniformit\`a e precisione delle linee dei reticoli. Tuttavia, il
processo di fabbricazione presenta ancora gravi difficolt\`a e, con un ispezione
al microscopio elettronico, si notano molti difetti strutturali, come
deformazioni (figura~\ref{fig:deformazioni}), galvanizzazione incompleta di
alcuni settori (figura~\ref{fig:galvanizzazione}), e un ciclo di lavoro
lontano dal valore nominale (misurato tra \num{0.35} e \num{0.6}).

\begin{figure}[htb]
    \centering
    \begin{subfigure}[b]{.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{gfx/Au_Grating_003.png}
    \caption{}
    \label{fig:deformazioni}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{gfx/Au_Grating_010.png}
    \caption{}
    \label{fig:galvanizzazione}
    \end{subfigure}
    \caption[Immagini di un reticolo al microscopio elettronico.]{Immagini al microscopio elettronico di un reticolo
        d'oro. Si evidenziano deformazioni nelle strutture del
        reticolo~(\ref{fig:deformazioni}). Alcune aree del reticolo mostrano una crescita incompleta delle
        strutture~(\ref{fig:galvanizzazione}). Le lamelle d'oro sono
    sostenute da una struttura di SU-8.}
\end{figure}
La visibilit\`a massima raggiunta per entrambi gli esperimenti risulta
distante dal massimo teorico del \SI{26}{\percent}, attestandosi in media tra il
\SI{5}{\percent} e il \SI{6}{\percent} (figure~\ref{fig:visibility100}
e~\ref{fig:visibility120}).

\begin{figure}[htb]
    \centering
    \input{gfx/visibility_visibility_100kev.pgf}
    \caption[Visibilit\`a a \SI{100}{\kilo\eV}.]{Visibilit\`a dell'interferometro con energia nominale di
        \SI{100}{\kilo\eV}. Esposizione di \SI{10}{\second} per \num{24}
    punti di fase.}
    \label{fig:visibility100}
\end{figure}
\begin{figure}[htb]
    \centering
    \input{gfx/visibility_S00618.pgf}
    \caption[Visibilit\`a a \SI{120}{\kilo\eV}.]{Visibilit\`a dell'interferometro con energia nominale di
        \SI{120}{\kilo\eV}. Esposizione di \SI{10}{\second} per \num{24}
    punti di fase.}
    \label{fig:visibility120}
\end{figure}

\subsection{Immagini}
Per bilanciare la scarsa visibilit\`a \`e necessario dunque aumentare il
numero di fotoni, ovvero il tempo di esposizione, e usare campioni 
che interagiscano pi\`u intensamente.

In figura~\ref{fig:screw} si osserva una scansione di \num{100} linee su 
\SI{1}{\centi\metre} di
una vite di zinco con \num{24} punti di fase e \SI{15}{\second} di
esposizione per punto, realizzata con l'interferometro a~\SI{100}{\kilo\eV}.
\`E stato possibile ricostruire le tre
immagini complementari descritte nel paragrafo~\ref{sec:imaging}.
Nell'immagine differenziale di fase si evidenziano particolarmente i
contorni dell'oggetto, dove la derivata~\eqref{eq:refraction.angle} \`e
massima.

\begin{figure}[hbt]
    \centering
    \input{gfx/images_S00052.pgf}
    \caption[Radiografia di una vite di zinco.]{Radiografia di una vite di zinco.
        L'immagine \`e stata
        acquisita con la scansione verticale di \SI{1}{\centi\metre} con \num{100} linee,
        \num{24} punti di fase per linea e un'esposizione di \SI{15}{\second} 
    per punto.}
    \label{fig:screw}
\end{figure}
L'immagine di un chip elettronico~\ref{fig:chip}, realizzata con gli stessi parametri della 
precedente, evidenzia ancora la complementariet\`a dei segnali, con i punti
di saldatura pi\`u evidenti anche al di sotto dei resistori.

\begin{figure}[hbt]
    \centering
    \input{gfx/images_S00075_S00071.pgf}
    \caption[Radiografia di un chip elettronico.]{Radiografia di un chip elettronico.
        L'immagine \`e stata
        acquisita con la scansione di \SI{2}{\centi\metre} con \num{200} linee,
        \num{24} punti di fase per linea e un'esposizione di \SI{15}{\second} 
    per punto.}
    \label{fig:chip}
\end{figure}
Uno dei problemi pi\`u rilevanti dal punto di vista teorico per il
funzionamento di questo interferometro alle energie in esame \`e quello del
contributo dell'effetto Compton, che oltre i~\SI{100}{\kilo\eV} diventa
dominante rispetto all'effetto fotoelettrico per molti materiali.
Infatti, sia l'assorbimento che la riduzione di visibilit\`a
(paragrafo~\ref{sec:imaging}), sono influenzate dalle propriet\`a di
\emph{scattering} del materiale. La seconda direttamente, il primo come
conseguenza della distribuzione angolare molto ampia della sezione d'urto
Compton, che si traduce in \emph{estinzione} del fascio.
Inoltre, nella teoria esposta nel capitolo~\ref{ch:teoria}, l'effetto
Compton non \`e considerato in quanto l'equazione
lineare~\ref{eq:helmoltz.spacetime}
esclude ogni accoppiamento tra componenti di frequenza diversa, e quindi
ogni fenomeno di \emph{scattering} inelastico.

Occorre tuttavia considerare che la presenza dei reticoli
implica una selezione molto
stringente sugli angoli di \emph{scattering} che raggiungono il
rivelatore~\cite{wilks1987principles}.
Quantitativamente, l'effetto Compton \`e soppresso dalla presenza dei
reticoli di un fattore
$p_2 / 2D_j \approx 10^{-5}$, quindi si pu\`o dire che tale fenomeno
contribuisce essenzialmente al solo assorbimento come estinzione
del fascio.

\begin{figure}[hbt]
    \centering
    \input{gfx/images_S00613.pgf}
    \caption[Radiografia di uno spessore di polistirolo.]{Radiografia di uno spessore di~\SI{5}{\centi\metre} di
polistirolo con al centro un cilindro di acciaio. L'immagine \`e stata
acquisita con la scansione di \num{6} linee con \num{24} punti di fase e un'esposizione di \SI{10}{\second}
per punto.}
    \label{fig:foam}
\end{figure}
\begin{figure}[hbt]
    \centering
    \input{gfx/profile_S00613.pgf}
    \caption[Contrasto in assorbimento e campo
    oscuro.]{Profilo della figura~\ref{fig:foam}. I materiali leggeri sono
poco assorbenti ma possono dare un forte contrasto di riduzione della
visibilit\`a.}
    \label{fig:profile}
\end{figure}
Con l'interferometro a \SI{120}{\kilo\eV} \`e stato possibile osservare
questa differenza con un campione di grande disomogeneit\`a su scala
microscopica, come un
rettangolo di
schiuma di polistirolo di \SI{5}{\centi\metre} di spessore
(figura~\ref{fig:foamsample}), che fornisce un
\begin{figure}[hbt]
    \centering
    \input{gfx/foam_sample.eepic}
    \caption[Campione di polistirolo e acciaio.]{Schema del campione di
        polistirolo lungo~\SI{5}{\centi\metre} in direzione del fascio, con
    un piccolo cilindro di acciaio all'interno.}
    \label{fig:foamsample}
\end{figure}
contrasto ridotto in assorbimento, attorno al \SI{2}{\percent}, essendo un
materiale leggero, ma molto intenso come riduzione della
visibilit\`a, circa del \SI{20}{\percent}, perch\'e disomogeneo su scala
microscopica. Il profilo in figura~\ref{fig:profile} mostra la transizione
tra aria, polistirolo e acciaio nelle due modalit\`a.
Anche questa immagine di fase
evidenzia chiaramente il bordo del parallelepipedo, ma il rumore \`e troppo
ampio per poter determinare quantitativamente 
l'indice di rifrazione.

\section{Conclusioni}
Un primo obiettivo di questo progetto, consistente nella progettazione e nella
realizzazione di due interferometri compatti ad energie finora mai
esplorate, pu\`o dirsi pienamente
raggiunto.
L'illuminazione \emph{edge-on} dei reticoli apre la strada
all'interferometria a raggi X a energie superiori a~\SI{100}{\kilo\eV} su
sorgenti convenzionali. Inoltre \`e possibile distribuire con facilit\`a le
strutture lungo archi di circonferenza per estendere il campo visivo in
orizzontale anche per geometrie compatte.

L'alto grado di collimazione del fascio \`e un altro potenziale vantaggio
per ridurre artefatti e scattering in direzione ortogonale. Il sistema
MicroDose di Philips per la mammografia riporta una notevole riduzione della
dose con un sistema simile, che prevede la scansione di sottili
fenditure per ottenere un'immagine completa del
campione~\cite{Aslund2007}.

Non \`e ancora possibile, soprattutto per via delle difficolt\`a
tecniche 
nella fabbricazione dei reticoli, riportare risultati quantitativi per
quanto riguarda i coefficienti di interazione $\beta$ e $\delta$ o stabilire
un miglioramento del rapporto segnale/rumore nelle immagini di fase. 
Tuttavia, i benefici derivanti dalla
complementariet\`a dei segnali di assorbimento, fase e \emph{scattering} sono disponibili anche in questo
regime. 
Inoltre, alcune piccole sezioni dei reticoli mostrano gi\`a ora una visibilit\`a
notevolmente
pi\`u alta che, se raggiunta in modo omogeneo su tutta l'area porterebbe a
una diminuzione del tempo di esposizione di un ordine di grandezza a
parit\`a di qualit\`a dell'immagine.

Ci\`o apre quindi la strada a future applicazioni nel campo della
diagnosi o della sicurezza.

%************************************************
\chapter{Teoria}\label{ch:teoria}
%************************************************
\section{Interazione dei raggi X con la materia}
L'interazione dei raggi X con la materia è ciò che rende tali onde
elettromagnetiche particolarmente interessanti come sonda delle proprietà
degli oggetti. Essa è infatti sufficientemente forte da permettere di
ricavare informazioni sulla materia attraversata, ma al tempo stesso
abbastanza debole da permettere di esplorarne l'intero volume.

Si introducono quindi brevemente
le approssimazioni che permettono di
ricavare alcune proprietà della materia dall'osservazione con raggi X a
partire dalle equazioni di Maxwell~\cite{jackson_classical_1999, Paganin2006b}
\begin{align}
    \nabla \cdot \vec{D} & = \rho \label{eq:gauss}\\
    \nabla \cdot \vec{B} & = 0 \label{eq:nomonopoles}\\
    \nabla \times \vec{E} + \frac{\partial}{\partial t} \vec{B} & = 0
    \label{eq:induction}\\
    \nabla \times \vec{H} - \frac{\partial}{\partial t} \vec{D} & = \vec{J}
    \label{eq:ampere}.
\end{align}
I campi $\vec{E}$ (elettrico), $\vec{H}$ (magnetico), $\vec{D}$ (spostamento
elettrico) e $\vec{B}$ (induzione magnetica), cos\`i come le densit\`a di
carica $\rho$ e di corrente $\vec{J}$ dipendono in generale dalle coordinate
tridimensionali dello spazio e dal tempo.

\subsection{Assenza di sorgenti}
Si assume innanzitutto che non ci siano densit\`a di carica o di corrente,
ovvero $\rho = 0$ e $\vec{J} = 0$.
Ne consegue che il campo elettrico e magnetico si possono disaccoppiare.

\subsection{Materiali lineari, isotropi e non magnetici}
La prima semplificazione che agevola la trattazione delle equazioni di
Maxwell riguarda le propriet\`a dei materiali. Consideriamo solo materiali
isotropi e lineari, per cui $\vec{D}(t, \vec{x}) = \varepsilon(\vec{x})
\vec{E}(t, \vec{x})$, e non magnetici, $\vec{B} = \mu_0 \vec{H}$. Si
noti che \`e esclusa anche la dipendenza esplicita dal tempo del coefficiente di permittivit\`a elettrica $\varepsilon$.

Queste approssimazioni escludono un'ampia categoria di fenomeni non lineari
e materiali non isotropi, come ad esempio i cristalli, ma permettono di
riscrivere le equazioni delle onde per il campo elettrico e magnetico in
forma pi\`u semplice.

Calcolando il rotore della~\eqref{eq:induction} e
sostituendo la~\eqref{eq:ampere} si ottiene l'equazione delle onde per il
campo elettrico, e con procedura analoga quella per il campo magnetico
\begin{align*}
    \left(\varepsilon \mu_0 \frac{\partial^2}{\partial t^2} - \nabla^2 \right)
    \vec{E} &= - \nabla(\nabla \cdot \vec{E}) \\
    \left(\varepsilon \mu_0 \frac{\partial^2}{\partial t^2} - \nabla^2 \right)
    \vec{H} &= \frac{1}{\varepsilon}\nabla \varepsilon \times (\nabla \times
    \vec{H}).\\
\end{align*}

\subsection{Teoria scalare}
Le due equazioni delle onde mescolano ancora le componenti cartesiane dei
campi nel secondo membro. Se la distribuzione dei centri d'interazione, e
cio\`e la densit\`a elettronica, varia
lentamente rispetto alla lunghezza d'onda dei raggi X, possiamo trascurare
tali derivate ed ottenere equazioni completamente disaccoppiate.
Considerando una sola delle componenti si pu\`o quindi trattare l'equazione scalare
\begin{equation}
    \left( \varepsilon(\vec{x}) \mu_0 \frac{\partial^2}{\partial t^2} - \nabla^2
    \right) \psi(t, \vec{x}) = 0.\label{eq:helmoltz.spacetime}
\end{equation}
\`E conveniente prendere la trasformata di Fourier del campo scalare. La
distinzione tra la trasformata e il campo originale \`e resa evidente nella
notazione solo
dall'argomento della funzione.
\begin{equation*}
    \psi(t, \vec{x}) =
    \frac{1}{\sqrt{2\pi}}\int_{0}^{\infty}\psi(\omega, \vec{x})
    e^{-i \omega t} \de{\omega}.
\end{equation*}
L'equazione~\eqref{eq:helmoltz.spacetime} si pu\`o riscrivere quindi, con
$\partial_t = - i\omega = -i c k$, e includendo una possibile
dipendenza della permittivit\`a dalla frequenza
\begin{equation*}
    \left( \varepsilon_\omega(\vec{x}) \mu_0 c^2 k^2 + \nabla^2
    \right) \psi(\omega, \vec{x}) = 0.
\end{equation*}
Introducendo infine l'indice di rifrazione $n_\omega =
c\sqrt{\varepsilon_\omega\mu_0}$ si arriva al risultato principale del
capitolo, ovvero l'equazione di Helmoltz
\begin{equation}
    \left( n_\omega^2(\vec{x}) k^2 + \nabla^2
    \right) \psi(\omega, \vec{x}) = 0.\label{eq:helmoltz.fourier}
\end{equation}

\subsection{Piccole deviazioni}
Sia $\psi$ un'onda piana monocromatica che viaggia lungo l'asse
$z$ attraverso un oggetto che si estende tra $z = 0$ e $z = z_0$
(figura~\ref{fig:propagation}).
\begin{figure}[htb]
    \centering
    \input{gfx/interaction.eepic}
    \caption[Propagazione di un'onda piana.]{Un'onda piana si propaga lungo l'asse $z$ attraverso un'oggetto
        che si estende da $z = 0$ a $z = z_0$.}
    \label{fig:propagation}
\end{figure}
Si vuole
calcolare il campo $\psi(\omega, x,y,z_0)$ determinato dalla perturbazione
introdotta dal campione. L'essenza della presente approssimazione \`e che le
interazioni sono sufficientemente deboli da deviare in modo trascurabile il
percorso dei raggi X, che quindi seguono lo stesso percorso rettilineo che
seguirebbero se il campione non ci fosse. Nell'ambito dell'ottica
ondulatoria, si pu\`o definire il percorso come la retta indicata dal
gradiente $\nabla\psi$.

I cambiamenti nella fase e nell'ampiezza dell'onda si possono cos\`i
calcolare come differenze accumulate lungo una retta parallela all'asse
$z$ a mano a mano che il campo attraversa
il mezzo.

\subsection{Approssimazione parassiale}
Concludiamo con l'approssimazione parassiale, che permette finalmente
di collegare l'indice di rifrazione di un campione con le propriet\`a
dei raggi X che lo attraversano.
Si ponga 
\begin{equation*}
 \psi(\omega, x, y, z) = \tilde{\psi}(\omega, x, y, z) \exp(ikz),    
\end{equation*}
ovvero
si supponga che la soluzione all'equazione~\eqref{eq:helmoltz.fourier} sia il
prodotto di una perturbazione $\tilde\psi$ e dell'onda piana che si propaga
liberamente. L'equazione di Helmoltz si pu\`o quindi riscrivere,
con $\nabla_\perp^2 = \partial_x^2 + \partial_y^2$,
\begin{equation*}
    \left[ 2ik \frac{\partial}{\partial z} + \nabla_\perp^2 +
    \frac{\partial^2}{\partial z ^2} + k^2 (n^2_\omega(\vec{x}) - 1)
\right]\tilde{\psi}(\omega, \vec{x}) = 0.
\end{equation*}
L'approssimazione parassiale consiste nel considerare le variazioni del
fascio lungo $z$ come trascurabili rispetto a quelle lungo $x$ e $y$, ovvero
nel tralasciare il termine con le derivate seconde $\partial_z^2$.

Possiamo inoltre tralasciare il termine con $\nabla_\perp^2$, essendo questo
termine responsabile degli accoppiamenti tra traiettorie vicine, che abbiamo
escluso nel paragrafo precedente
\begin{equation*}
    \frac{\partial}{\partial z} \tilde{\psi}(\omega, x, y, z) =
    \frac{k}{2i}(1 - n^2_\omega(\vec{x}))\tilde{\psi}(\omega, x, y, z).
\end{equation*}
Per i raggi X, l'indice di rifrazione \`e normalmente molto vicino a 1, e si
esprime spesso nella forma complessa
\begin{equation*}
    n_\omega = 1 - \delta_\omega + i\beta_\omega.
\end{equation*}
Tralasciando quindi i termini di secondo grado in $\delta$ e $\beta$, si
ottiene la soluzione
\begin{equation*}
    \tilde{\psi}(\omega, x,y,z_0) = \exp\left\{-ik\int_{z=0}^{z=z_0}(\delta_\omega - i
\beta_\omega) \de z \right\} \tilde{\psi}(\omega, x,y,0)
\end{equation*}
Dove si possono distinguere i due contributi dovuti allo spostamento della
fase introdotta dall'oggetto
\begin{equation*}
    \Delta\varphi(x, y) = -k \int \delta_\omega(x, y, z) \de z
    %\label{eq:phase_deviation}
\end{equation*}
e, identificando il quadrato della $\psi$ con l'intensit\`a, all'assorbimento
della radiazione secondo la legge di Beer-Lambert
\begin{equation}
    I(\omega, x, y, z_0) = \exp\left\{-2k\int \beta_\omega(x, y, z) \de
    z\right\}
    I(\omega, x, y, 0).\label{eq:beer-lambert}
\end{equation}
\`E utile notare che i coefficienti dipendono dalle propriet\`a del
materiale di numero atomico $Z$~\cite{knoll2000radiation,Momose2005} a basse energie, e
lontano dalle linee caratteristiche di assorbimento, secondo $\beta \propto Z^3 / k^4$ e $\delta \propto Z / k^2$, e
quindi ad alte energie il vantaggio introdotto dalle immagini di fase \`e
maggiore, in quanto l'interazione di fase \`e relativamente pi\`u importante.

\section{Propagazione libera ed effetto Talbot}

\subsection{Il propagatore di Fresnel}
L'equazione di Helmoltz~\eqref{eq:helmoltz.fourier} fornisce 
anche la soluzione alla propagazione del fronte d'onda nel vuoto, con
$n \equiv 1$. Le onde piane $\psi(\omega, \vec{x}) =
e^{i\vec{k}\cdot \vec{x}}$ sono soluzioni, con $k^2 = k_x^2 + k_y^2 + k_z^2
= c^2 \omega^2$. Si possono quindi scrivere, isolando $k_z$, nella forma
\begin{equation}
    \psi(\omega, x, y, z) = e^{i(k_x x + k_y y)}e^{iz\sqrt{k^2 - k_x^2 -
    k_y^2}}.\label{eq:free.propagation}
\end{equation}
Da questa scrittura \`e evidente che l'onda in $z = 0$ si propaga con il
fattore $\exp(iz\sqrt{k^2 - k_x^2 - k_y^2})$.
Quindi in generale, data una soluzione
dell'equazione~\eqref{eq:helmoltz.fourier} per $z = 0$, possiamo scomporla,
attraverso la trasformata di Fourier $\Fxy$ rispetto a $x$ e $y$, in
onde piane, ciascuna delle quali si
propaga secondo la~\eqref{eq:free.propagation}, e infine tornare al dominio
spaziale con la trasformata di Fourier inversa. Schematicamente:
\begin{equation*}
    \psi(\omega, x, y, z_0) = \Fxy^{-1}e^{i z_0 \sqrt{k^2 - k_x^2 -
    k_y^2}}\Fxy    \psi(\omega, x, y, 0) 
\end{equation*}
Introduciamo anche in questa formula l'approssimazione parassiale, secondo
cui le componenti trasverse del fascio sono molto piccole rispetto a quella
in direzione $z$, ovvero $|k_x|, |k_y| \ll |k_z|$, per espandere la radice
$\sqrt{k^2 - k_x^2 - k_y^2} \approx k - \frac{k_x^2 + k_y^2}{2k}$
e ottenere la forma finale del propagatore
\begin{equation}
    \psi(\omega, x, y, z_0) = e^{i k z_0}\Fxy^{-1}e^{-i z_0\frac{k_x^2 +
    k_y^2}{2k}}\Fxy\psi(\omega, x, y, 0) . \label{eq:paraxial.propagator}
\end{equation}

\subsection{L'effetto Talbot}
Il fenomeno ottico scoperto nel 1836~\cite{Talbot1836} \`e alla base del funzionamento
dell'interferometro. Talbot osserv\`o infatti che un fronte d'onda periodico
che si propaga nel vuoto ricostruisce a distanze fissate un'immagine di s\'e
stesso.

Sia infatti il campo monocromatico $\psi(\omega, x, z=0)$ periodico lungo
$x$ con periodo $p_1$. Trascuriamo per semplicit\`a la dipendenza dalla
direzione ortogonale $y$. Allora, secondo la procedura descritta
in~\eqref{eq:paraxial.propagator}, prendiamo la trasformata di Fourier lungo
$x$, che sar\`a della forma
\begin{equation*}
    \Fx\psi(\omega, x, 0) = \sum_j \psi_j(\omega)\delta(k_x -
    k_{xj}),
\end{equation*}
con $k_{xj} = 2\pi j/ p_1$. Sostituendo
nella~\eqref{eq:paraxial.propagator}, si vede che il propagatore
si riduce quindi a $\exp(-i z_0 k_{xj}^2 / 2k)$ che \`e uguale a \num{1} per
alcune distanze, dette distanze di Talbot 
\begin{equation*}
    \Delta_n = n \frac{p_1^2}{2 \lambda} \qquad n \in \mathbb{N}.
\end{equation*}
Questo effetto, detto effetto Talbot \emph{intero}, si realizza mediante l'inserimento
nel fascio di un reticolo che assorba la radiazione. \`E anche possibile
introdurre, senza dimezzare cos\`i l'intensit\`a, una periodicit\`a nella
fase attraverso un reticolo
$G1$ di periodo $p_1$ le cui linee cambiano
la fase dell'onda originale di un fattore $\pi$. In base allo stesso
principio, si osserva
l'effetto Talbot \emph{frazionario}, per cui un'immagine del reticolo si forma, con
periodo dimezzato, alle distanze di Lohmann~\cite{Lohmann1971}
\begin{equation*}
    D_j = \left(j - \frac{1}{2}\right) \frac{p_1^2}{4 \lambda} \qquad
    j\in\mathbb{N}.
\end{equation*}
In figura~\ref{fig:talbotcarpet} si mostra la propagazione di un fronte
d'onda periodico
simulata in base all'equazione~\ref{eq:paraxial.propagator}. \`E possibile
estendere la simulazione a tutto l'interferometro, ma per la scarsa
qualit\`a dei reticoli (capitolo~\ref{ch:esperimenti}) non \`e ancora possibile
riportare confronti quantitativi. Sono oggetto di studi recenti anche
simulazioni con tecniche Monte Carlo, che includono anche i fenomeni pi\`u
tipicamente \emph{particellari} dell'interazione dei raggi
X~\cite{Bartl2010b,Peter2013}.
\begin{figure}[htb]
    \centering
    \input{gfx/talbotcarpet.pgf}
    \caption[Tappeto di Talbot.]{Simulazione del \emph{tappeto di Talbot},
    ovvero della propagazione di un fronte
    d'onda con periodicit\`a di fase. Si osserva la formazione di frange luminose alle
    distanze di Lohmann con periodo dimezzato rispetto al reticolo \G{1}.}
    \label{fig:talbotcarpet}
\end{figure}
